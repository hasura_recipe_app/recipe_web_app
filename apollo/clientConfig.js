import { InMemoryCache } from "apollo-cache-inmemory";

// FUNCTION FOR SETTING TOKEN IN LOCAL STORAGE
const getHeaders = () => {
  const headers = {};

  const token = localStorage.getItem("token");
  if (token) {
    headers.authorization = `Bearer ${token}`;
  }
  return headers;
};

// FUNCTION FOR CONNECTION WITH HASURA GRAPHQL ENGINE
export default function(context) {
  return {
    httpLinkOptions: {
      uri: "http://localhost:8080/v1/graphql",
      credentials: "same-origin",
      headers: getHeaders()
    },
    cache: new InMemoryCache(),
    wsEndpoint: "ws://localhost:8080/v1/graphql"
  };
}

// FUNCTION FOR USER AUTHORIZATION BASED ON TOKEN

export default function({ store, redirect }) {
  if (window.localStorage.getItem("token") === "") {
    return redirect("/signin");
  }
}

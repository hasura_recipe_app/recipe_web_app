export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  ssr: false,

  head: {
    titleTemplate: "%s - #1",
    title: "Gebeta Recipe",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.png" },
      {
        rel: "stylesheet",
        href:
          "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css"
      },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css2?family=Raleway:wght@100;300;400;500;600;900&display=swap"
      }
    ]
  },

  loading: { color: "#FF2E00", height: "3px" },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: "~/plugins/vuexPersisting.js"
    }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: ["~/components", { path: "~/components/Buttons/" }],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify"
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/apollo"],

  apollo: {
    cookieAttributes: {
      expires: 7
    },
    includeNodeModules: true,
    authenticationType: "Bearer",
    clientConfigs: {
      default: "~/apollo/clientConfig.js"
    },
    tokenName: "token",
    persisting: true
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/style.scss"],
    treeShake: true
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
};

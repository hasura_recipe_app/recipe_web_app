import createPersistedState from "vuex-persistedstate";

// FOR STATE DATA PERSISTING AND STORING IN TO SESSION STORAGE
export default ({ store }) => {
  createPersistedState({
    key: "CMP",
    storage: window.sessionStorage
  })(store);
};

// STATE FOR RECIPE
export const state = () => ({
  recipe: [],
  searchedRecipe: [],
  homeRecipe: [],
  favoriteRecipe: []
});

// GETTERS FOR RECIPE
export const getters = {
  getFavoriteRecipe: state => {
    return state.favoriteRecipe;
  },
  getRecipe: state => {
    return state.recipe;
  },
  getSearchedRecipe: state => {
    return state.searchedRecipe;
  },
  getHomeRecipe: state => {
    return state.homeRecipe;
  }
};

// SETTERS FOR RECIPE
export const mutations = {
  fetchFavoriteRecipe(state, recipe) {
    state.favoriteRecipe = recipe;
  },
  setFavoriteRecipe(state, recipe) {
    state.favoriteRecipe.push(recipe);
  },
  setHomeRecipe(state, recipe) {
    state.homeRecipe = recipe;
  },
  setSearchedRecipe(state, recipe) {
    state.searchedRecipe = recipe;
  },
  setRecipe(state, recipe) {
    state.recipe.push(recipe);
  },
  fetchRecipeD(state, recipe) {
    state.recipe = recipe;
  },
  editRecipe(state, { id, newRecipe }) {
    const yIndex = state.recipe.findIndex(recipe => recipe.id === id);
    const hIndex = state.homeRecipe.findIndex(recipe => recipe.id === id);
    const sIndex = state.searchedRecipe.findIndex(recipe => recipe.id === id);

    if (yIndex != -1) state.recipe.splice(yIndex, 1, newRecipe);
    if (hIndex != -1) state.homeRecipe.splice(hIndex, 1, newRecipe);
    if (sIndex != -1) state.searchedRecipe.splice(sIndex, 1, newRecipe);
  },
  deleteRecipe(state, id) {
    const index = state.recipe.findIndex(recipe => recipe.id === id);
    state.recipe.splice(index, 1);
  },
  deleteFavoriteRecipe(state, id) {
    const index = state.favoriteRecipe.findIndex(recipe => recipe.id === id);
    state.favoriteRecipe.splice(index, 1);
  },
  addComment(state, recipe) {
    const index = state.recipe.findIndex(recipe => recipe.id === recipe.id);
    state.recipe.splice(index, 1);
    state.recipe.push(recipe);
  },
  removeStateR(state) {
    state.recipe = [];
    state.searchedRecipe = [];
    state.homeRecipe = [];
    state.favoriteRecipe = [];
  }
};

// ACTIONS FOR RECIPE
export const actions = {
  setFavoriteRecipeA({ commit }, recipe) {
    commit("setFavoriteRecipe", recipe);
  },
  fetchFavoriteRecipeA({ commit }, recipe) {
    commit("fetchFavoriteRecipe", recipe);
  },
  setHomeRecipeA({ commit }, recipe) {
    commit("setHomeRecipe", recipe);
  },
  setSearchedRecipeA({ commit }, recipe) {
    commit("setSearchedRecipe", recipe);
  },
  setRecipeA({ commit }, recipe) {
    commit("setRecipe", recipe);
  },
  fetchRecipeD({ commit }, recipe) {
    commit("fetchRecipeD", recipe);
  },
  deleteRecipeA({ commit }, id) {
    commit("deleteRecipe", id);
  },
  deleteFavoriteRecipeA({ commit }, id) {
    commit("deleteFavoriteRecipe", id);
  },
  editRecipeA({ commit }, payload) {
    commit("editRecipe", payload);
  },
  addCommentA({ commit }, recipe) {
    commit("addComment", recipe);
  },
  removeStateRA({ commit }) {
    commit("removeStateR");
  }
};

// STATE FOR USER
export const state = () => ({
  userId: "",
  token: "",
  username: "",
  email: "",
  photoPath: "",
  role: ""
});

// GETTERS FOR USER STATE
export const getters = {
  getAccessToken: state => {
    return state.token;
  },
  getUserId: state => {
    return state.userId;
  },
  getUsername: state => {
    return state.username;
  },
  getEmail: state => {
    return state.email;
  },
  getPhotoPath: state => {
    return state.photoPath;
  },
  getRole: state => {
    return state.role;
  },
  isAuthenticated: state => {
    return state.token !== "";
  }
};

// MUTATION OR SETTERS FOR USER
export const mutations = {
  setAccessToken(state, token) {
    state.token = token;
  },
  setUserId(state, id) {
    state.userId = id;
  },
  setUsername(state, username) {
    state.username = username;
  },
  setEmail(state, email) {
    state.email = email;
  },
  setPhotoPath(state, photoPath) {
    state.photoPath = photoPath;
  },
  setRole(state, role) {
    state.role = role;
  },
  removeState(state) {
    state.token = "";
    state.userId = "";
    state.username = "";
    state.email = "";
    state.photoPath = "";
    state.role = "";
  }
};

// ACTIONS FOR USER STATE
export const actions = {
  setAccessTokenA({ commit }, token) {
    commit("setAccessToken", token);
  },
  setUserIdA({ commit }, userId) {
    commit("setUserId", userId);
  },
  setUsernameA({ commit }, username) {
    commit("setUsername", username);
  },
  setEmailA({ commit }, email) {
    commit("setEmail", email);
  },
  setPhotoPathA({ commit }, photoPath) {
    commit("setPhotoPath", photoPath);
  },
  setRoleA({ commit }, role) {
    commit("setRole", role);
  },
  removeStateA({ commit }) {
    commit("removeState");
  }
};
